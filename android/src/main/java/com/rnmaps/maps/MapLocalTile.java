package com.rnmaps.maps;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.Arrays;
import java.util.List;

import java.lang.Math;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class MapLocalTile extends MapFeature {


    class MapLocalTileProvider implements TileProvider {
    private static final int BUFFER_SIZE = 16 * 1024;
    private int tileSize;
    private String pathTemplate;

    private AssetManager assets;

    public MapLocalTileProvider(int tileSizet, String pathTemplate, AssetManager assets) {
        this.tileSize = tileSizet;
        this.pathTemplate = pathTemplate;
        this.assets = assets;
    }

    @Override
    public Tile getTile(int x, int y, int zoom) {

        byte[] image = readTileImage(x, y, zoom);

            int zoomDif = 0;
            int zoomRatio = 1;

        // nothing found at current zoom level, check subsequent zooms until a tile is found
        // tile index offsets are 2^n so we adjust the x,y coordinates by half each zoom level
        while (image == null) {
            zoomDif++;
            zoomRatio = (int)Math.pow(2, zoomDif);
            image = readTileImage(x / zoomRatio, y / zoomRatio, zoom - zoomDif);
        }

        if (zoomDif > 0) {
                Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);

                // create new bmp as cropped section of the larger tile
                // the remainder of the coordinates multiplied by the resolution of the crop gives us
                // the correct offset
                int res = 512 / zoomRatio;
                Bitmap resizedMap = Bitmap.createBitmap(bmp, (x % zoomRatio) * res, (y % zoomRatio) * res, res, res);

                // google maps tile expects byte stream, so make a new one
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
            resizedMap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            if (stream != null) try { stream.close(); } catch (Exception ignored) { }

            return new Tile(this.tileSize, this.tileSize, byteArray);
        }

        return image == null ? TileProvider.NO_TILE : new Tile(this.tileSize, this.tileSize, image);
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public void setTileSize(int tileSize) {
        this.tileSize = tileSize;
    }

    private byte[] readTileImage(int x, int y, int zoom) {
        InputStream in = null;
            ByteArrayOutputStream buffer = null;

        try {
                in = this.assets.open(getTileFilename(x, y, zoom));

            String tileFilename = getTileFilename(x, y, zoom);

            buffer = new ByteArrayOutputStream();

                int nRead;
            byte[] data = new byte[BUFFER_SIZE];

            while ((nRead = in.read(data, 0, BUFFER_SIZE)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();

            return buffer.toByteArray();
        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) try { in.close(); } catch (Exception ignored) { }
            if (buffer != null) try { buffer.close(); } catch (Exception ignored) { }
        }
    }

    private String getTileFilename(int x, int y, int zoom) {
            String s = this.pathTemplate
            .replace("{x}", Integer.toString(x))
            .replace("{y}", Integer.toString(y))
            .replace("{z}", Integer.toString(zoom));
        return s;
    }
}

    private TileOverlayOptions tileOverlayOptions;
    private TileOverlay tileOverlay;
    private MapLocalTile.MapLocalTileProvider tileProvider;

    private String pathTemplate;
    private float tileSize;
    private float zIndex;

    private Context context;
    private AssetManager assets;


    public MapLocalTile(Context context) {
    super(context);
    this.context = context;
    this.assets = context.getAssets();
}

    public void setPathTemplate(String pathTemplate) {
    this.pathTemplate = pathTemplate;
    if (tileProvider != null) {
        tileProvider.setPathTemplate(pathTemplate);
    }
    if (tileOverlay != null) {
        tileOverlay.clearTileCache();
    }
}

    public void setZIndex(float zIndex) {
    this.zIndex = zIndex;
    if (tileOverlay != null) {
        tileOverlay.setZIndex(zIndex);
    }
}

    public void setTileSize(float tileSize) {
    this.tileSize = tileSize;
    if (tileProvider != null) {
        tileProvider.setTileSize((int)tileSize);
    }
}

    public void setUseAssets(boolean useAssets) {
    //        this.useAssets = useAssets;
}

    public TileOverlayOptions getTileOverlayOptions() {
    if (tileOverlayOptions == null) {
        tileOverlayOptions = createTileOverlayOptions();
    }
    return tileOverlayOptions;
}

    private TileOverlayOptions createTileOverlayOptions() {
        TileOverlayOptions options = new TileOverlayOptions();
    options.zIndex(zIndex);

    this.tileProvider = new MapLocalTile.MapLocalTileProvider((int)this.tileSize, this.pathTemplate, assets);
    options.tileProvider(this.tileProvider);
    options.fadeIn(false);
    return options;
}

@Override
public Object getFeature() {
    return tileOverlay;
}

@Override
public void addToMap(Object map) {
    this.tileOverlay = ((GoogleMap) map).addTileOverlay(getTileOverlayOptions());
}

@Override
public void removeFromMap(Object map) {
    tileOverlay.remove();
}
}
