//
//  AIRMapLocalTileOverlay.m
//  Pods-AirMapsExplorer
//
//  Created by Peter Zavadsky on 04/12/2017.
//  Adapted by Lachlan Russell on 24/07/2021.
//

#import "AIRMapLocalTileOverlay.h"
#import <math.h>

@interface AIRMapLocalTileOverlay ()

+ (void)replaceCoordinatesOf:(NSMutableString*)tileFilePath withX:(long)x withY:(long)y withZ:(long)z;

@end


@implementation AIRMapLocalTileOverlay

- (void)loadTileAtPath:(MKTileOverlayPath)path result:(void (^)(NSData *, NSError *))result {
    
    NSFileManager *fs = [NSFileManager defaultManager];
    NSMutableString *tileFilePath;
    
    tileFilePath = [self.URLTemplate mutableCopy];
    [AIRMapLocalTileOverlay replaceCoordinatesOf:tileFilePath withX:path.x withY:path.y withZ:path.z];
    
    int zoomDiff;
    int zoomRatio = 0;
    
    for (zoomDiff = 0; ![fs fileExistsAtPath: tileFilePath]; zoomDiff++) {
        zoomRatio = (int)powf(2.0, (float)zoomDiff);
        tileFilePath = [self.URLTemplate mutableCopy];
        [AIRMapLocalTileOverlay replaceCoordinatesOf:tileFilePath
                                               withX:path.x / zoomRatio
                                               withY:path.y / zoomRatio
                                               withZ:(int)fmax(0, path.z - zoomDiff)];
    }
    
    NSData* tile = [NSData dataWithContentsOfFile:tileFilePath];
    
    if (zoomDiff > 0) {
        UIImage *image = [UIImage imageWithData:tile];
        size_t width = CGImageGetWidth(image.CGImage);
        size_t res = width / zoomRatio;
        CGRect crop = CGRectMake((path.x % zoomRatio) * res, (path.y % zoomRatio) * res, res, res);
        CGImageRef croppedImage = CGImageCreateWithImageInRect(image.CGImage, crop);
        UIImage *newImage = [UIImage imageWithCGImage:croppedImage];
        newImage = [AIRMapLocalTileOverlay imageWithImage:newImage scaledToSize:CGSizeMake(width, width)];
        CGImageRelease(croppedImage);
        tile = UIImagePNGRepresentation(newImage);
    }
    result(tile, nil);
}

+ (void)replaceCoordinatesOf:(NSMutableString*)tileFilePath withX:(long)x withY:(long)y withZ:(long)z {
    [tileFilePath replaceOccurrencesOfString:@"{x}" withString:[NSString stringWithFormat:@"%li", (long)x] options:0 range:NSMakeRange(0, tileFilePath.length)];
    [tileFilePath replaceOccurrencesOfString:@"{y}" withString:[NSString stringWithFormat:@"%li", (long)y] options:0 range:NSMakeRange(0, tileFilePath.length)];
    [tileFilePath replaceOccurrencesOfString:@"{z}" withString:[NSString stringWithFormat:@"%li", (long)z] options:0 range:NSMakeRange(0, tileFilePath.length)];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
